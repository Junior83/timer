package njdevelopment;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import javazoom.jl.player.jlp;

import java.awt.Color;


public class Interface_Timer {

	private JFrame frame;
	private Timer timer;
	private JComboBox cbHoras, cbMinutos;
	private JLabel lblTimer;
	
	private int hora = 0; 
	private int minuto = 0;
	private int segundo = 0;
	private int velocidade = 1000;
	
//	public void alarme(){
//		
//		try {
//				String endereco = "/Users/juniorsilva/Music/iTunes/iTunes Media/Music/";
//				String banda = "Red Hot Chili Peppers";
//				String album = "Stadium Arcadium (Jupiter)";
//				String musica = "13 Wet Sand.mp3";
//
//				String tocar = endereco + banda +"/"+ album +"/"+ musica;
//				System.out.println("--> VOCÊ ESTÁ OUVINDO: " + musica + " <--");
//				
//				jlp alarme = new jlp(tocar);
//				alarme.play();
//
//		} catch (Exception e) {}
//	}
	
	public void iniciarTimer(){
		
		ActionListener action = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				segundo--;
				
				if(segundo == -1){
					segundo = 59;
					minuto--;
				}
				
				if(minuto == -1){
					minuto = 59;
					hora--;
				}
				
				if(hora == -1){
					hora = 0;
				}
				
				if(hora == 0 && minuto == 0 && segundo == 0){
					encerrarTimer();
					//alarme();
				}
				
				String hor = hora<10?"0"+hora:""+hora;
				String min = minuto<10?"0"+minuto:""+minuto;
				String seg = segundo<10?"0"+segundo:""+segundo;
				
				lblTimer.setText(hor+":"+min+":"+seg);
			}
		};
		timer = new Timer(velocidade, action);
		timer.start();
	}
	
	private void encerrarTimer(){
		
		hora = 0;
		minuto = 0;
		segundo = 0;
		lblTimer.setText("00:00:00");
		cbHoras.setSelectedItem(hora);
		cbMinutos.setSelectedItem(minuto);
		timer.stop();
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_Timer window = new Interface_Timer();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface_Timer() {
		initialize();
		iniciarTimer();
		encerrarTimer();
		
		for(int hora : Tempo.HORAS){
			cbHoras.addItem(hora);
		}
		
		for(int minuto : Tempo.MINUTOS){
			cbMinutos.addItem(minuto);
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 261, 162);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setTitle("Timer");
		
		cbHoras = new JComboBox();
		cbHoras.setBounds(6, 6, 73, 49);
		frame.getContentPane().add(cbHoras);
		
		cbMinutos = new JComboBox();
		cbMinutos.setBounds(125, 6, 73, 49);
		frame.getContentPane().add(cbMinutos);
		
		JLabel lblHora = new JLabel("Horas");
		lblHora.setBounds(82, 4, 63, 50);
		frame.getContentPane().add(lblHora);
		
		JLabel lblMinutos = new JLabel("Minutos");
		lblMinutos.setBounds(198, 4, 63, 50);
		frame.getContentPane().add(lblMinutos);
		
		lblTimer = new JLabel("00:00:00");
		lblTimer.setHorizontalAlignment(SwingConstants.CENTER);
		lblTimer.setFont(new Font("Lucida Grande", Font.BOLD, 30));
		lblTimer.setBounds(29, 49, 200, 50);
		frame.getContentPane().add(lblTimer);
		
		JButton btnPausar = new JButton("Pausar");
		btnPausar.setForeground(new Color(218, 165, 32));
		btnPausar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//========================================
				//BOTÃO PAUSAR
				if(!timer.isRunning() && hora == 0 && minuto == 0 && segundo == 0){
					encerrarTimer();
					
				}else if(timer.isRunning()){
					timer.stop();
					btnPausar.setText("Continuar");
					btnPausar.setForeground(Color.BLUE);
				}else{
					timer.restart();
					btnPausar.setText("Pausar");
					btnPausar.setForeground(new Color(218, 165, 32));
				}
				//========================================
			}
		});
		btnPausar.setBounds(82, 98, 96, 29);
		frame.getContentPane().add(btnPausar);
		
		JButton btnIniciar = new JButton("Iniciar");
		btnIniciar.setForeground(new Color(0, 128, 0));
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//========================================
				//BOTÃO INICIAR
				hora = cbHoras.getSelectedIndex();
				minuto = cbMinutos.getSelectedIndex();
				segundo = 0;
				timer.start();
				
				if(cbHoras.getSelectedIndex() == 0 && cbMinutos.getSelectedIndex() == 0){
					minuto = 1;
					cbMinutos.setSelectedIndex(minuto);
				}
				//========================================
			}
		});
		btnIniciar.setBounds(6, 98, 80, 29);
		frame.getContentPane().add(btnIniciar);
		
		JButton btnParar = new JButton("Zerar");
		btnParar.setForeground(new Color(165, 42, 42));
		btnParar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//========================================
				//BOTÃO ZERAR
				encerrarTimer();
				
				if(!timer.isRunning()){
					btnPausar.setText("Pausar");
					btnPausar.setForeground(new Color(218, 165, 32));
				}
				//========================================
			}
		});
		btnParar.setBounds(175, 98, 80, 29);
		frame.getContentPane().add(btnParar);
		
		JLabel lblNewLabel = new JLabel("<html>Created by NJdevelopment&copy;</html>");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 9));
		lblNewLabel.setBounds(118, 125, 137, 9);
		frame.getContentPane().add(lblNewLabel);
	}
}
